
import smtplib, ssl
from smtplib import SMTPAuthenticationError, SMTPException
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from constants import (
    SENDER_EMAIL,
    SENDER_PASSWORD,
    SMTP_SERVER_EMAIL,
    SMTP_SERVER_PORT,
)

def send_mail(
    email_subject,
    email_message,
    receiver_email,
    sender_mail=None,
    sender_mail_password=None,
    sender_server_smtp=None,
    port=SMTP_SERVER_PORT,
 

):
    
    message = MIMEMultipart()
    bodyMessage = email_message
    if sender_mail == None:
        sender_mail = SENDER_EMAIL
        sender_mail_password = SENDER_PASSWORD
        sender_server_smtp = SMTP_SERVER_EMAIL
        bodyMessage = MIMEText(email_message, "html")
    message["Subject"] = email_subject
    message["From"] = sender_mail
    message["To"] = receiver_email
    message.attach(MIMEText(bodyMessage))
    context = ssl.create_default_context()
    body=message.as_string()
    print(message)

    try:
        with smtplib.SMTP_SSL(sender_server_smtp, port, context=context) as server:
            server.login(sender_mail, sender_mail_password)
            server.sendmail(sender_mail, [receiver_email], body)
    except (SMTPException, SMTPAuthenticationError):
            print ("error")


