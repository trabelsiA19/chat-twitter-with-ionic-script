
def required_field_message(field):
    return {"message": f"{field} is required", "code": 400}
