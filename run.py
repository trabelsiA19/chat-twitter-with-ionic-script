from flask import Flask, jsonify
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from flask_smorest import Api
from flask_marshmallow import Marshmallow
from dotenv import load_dotenv

# setting up .env data #
load_dotenv(dotenv_path=".env")

# APIs

from send import app as subscription_app


app = Flask(__name__)
app.config["API_TITLE"] = "twitter"
app.config["API_VERSION"] = "v1.0.5"
app.config["OPENAPI_VERSION"] = "3.0.1"

# Marshmallow
ma = Marshmallow(app)

# CORS
CORS(app)

# flask-smorest
api = Api(app)

api.register_blueprint(subscription_app)


if __name__ == "__main__":
    app.config["DEBUG"] = True
    app.run()




